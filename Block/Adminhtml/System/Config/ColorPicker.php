<?php


namespace Tpots\Core\Block\Adminhtml\System\Config;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

/**
 * Class ColorPicker
 * @package Tpots\Core\Block\Adminhtml\System\Config
 */
class ColorPicker extends Field {

    /**
     * Colorpicker constructor.
     *
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        Context $context, array $data = []
    ) {
        parent::__construct($context, $data);
    }

    /**
     * @param AbstractElement $element
     *
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        $html  = $element->getElementHtml();
        $value = $element->getData('value');

        $html .= '<script type="text/javascript">
        require(["jquery","jquery/colorpicker/js/colorpicker"], function ($) {
            $(document).ready(function () {
                var $el = $("#' . $element->getHtmlId() . '");
                $el.css("backgroundColor", "' . $value . '");
                $el.ColorPicker({
                    color: "' . $value . '",
                    onChange: function (hsb, hex, rgb) {
                        $el.css("backgroundColor", "#" + hex).val("#" + hex);
                    }
                });
            });
        });
        </script>';

        return $html;
    }

}
