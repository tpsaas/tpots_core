<?php


namespace Tpots\Core\Plugin;

use Magento\Backend\Model\Menu\Builder\AbstractCommand;
use Tpots\Core\Helper\AbstractData;

/**
 * Class MoveMenu
 * @package Tpots\Core\Plugin
 */
class MoveMenu
{
    const TPOTS_CORE = 'Tpots_Core::menu';

    /**
     * @var AbstractData
     */
    protected $helper;

    /**
     * MoveMenu constructor.
     *
     * @param AbstractData $helper
     */
    public function __construct(AbstractData $helper)
    {
        $this->helper = $helper;
    }

    /**
     * @param AbstractCommand $subject
     * @param $itemParams
     *
     * @return mixed
     */
    public function afterExecute(AbstractCommand $subject, $itemParams)
    {
        if ($this->helper->getConfigGeneral('menu')) {
            if (strpos($itemParams['id'], 'Tpots_') !== false
                && isset($itemParams['parent'])
                && strpos($itemParams['parent'], 'Tpots_') === false) {
                $itemParams['parent'] = self::TPOTS_CORE;
            }
        } elseif ((isset($itemParams['id']) && $itemParams['id'] === self::TPOTS_CORE)
                || (isset($itemParams['parent']) && $itemParams['parent'] === self::TPOTS_CORE)) {
            $itemParams['removed'] = true;
        }

        return $itemParams;
    }
}
