

var config = {
    paths: {
        'tpots/core/jquery/popup': 'Tpots_Core/js/jquery.magnific-popup.min',
        'tpots/core/owl.carousel': 'Tpots_Core/js/owl.carousel.min',
        'tpots/core/bootstrap': 'Tpots_Core/js/bootstrap.min',
        tpIonRangeSlider: 'Tpots_Core/js/ion.rangeSlider.min',
        touchPunch: 'Tpots_Core/js/jquery.ui.touch-punch.min',
        tpDevbridgeAutocotplete: 'Tpots_Core/js/jquery.autocomplete.min'
    },
    shim: {
        "tpots/core/jquery/popup": ["jquery"],
        "tpots/core/owl.carousel": ["jquery"],
        "tpots/core/bootstrap": ["jquery"],
        tpIonRangeSlider: ["jquery"],
        tpDevbridgeAutocotplete: ["jquery"],
        touchPunch: ['jquery', 'jquery-ui-modules/core', 'jquery-ui-modules/mouse', 'jquery-ui-modules/widget']
    }
};
